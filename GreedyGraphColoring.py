def GreedyGraphColoring(graph, M):
    
    # GGC_inner code modified from https://www.geeksforgeeks.org/graph-coloring-set-2-greedy-algorithm/
    
    # Assigns colors (starting from 0) to all
    # vertices and prints the assignment of colors
    def GGC_inner(adj, V):

        result = [-1] * V

        # Assign the first color to first vertex
        result[0] = 0;


        # A temporary array to store the available colors.
        # True value of available[cr] would mean that the
        # color cr is assigned to one of its adjacent vertices
        available = [False] * V

        # Assign colors to remaining V-1 vertices
        for u in range(1, V):

            # Process all adjacent vertices and
            # flag their colors as unavailable
            for i in adj[u]:
                if (result[i] != -1):
                    available[result[i]] = True

            # Find the first available color
            cr = 0
            while cr < V:
                if (available[cr] == False):
                    break

                cr += 1

            # Assign the found color
            result[u] = cr

            # Reset the values back to false
            # for the next iteration
            for i in adj[u]:
                if (result[i] != -1):
                    available[result[i]] = False

        return result

    # This code is contributed by mohit kumar 29

    # The following code is created by Team Argonne for Group A
    
    g_lst = [[] for i in range(len(graph))]
    
    for key, value in graph.items():
        g_lst[key] = value
        
    #print(g_lst)
        
    return GGC_inner(g_lst, M)
