import numpy as np
import pandas as pd

def downloadTowerData(filename=None):
    """
    Function for reading tower csv file and returning coordinates.
    
    Input:
    
        filename - default: None
            If none, tower data read from the internet.
            'https://msu-cmse-courses.github.io/cmse495-SS22/Files/TowersRVN479554GVQ144174.csv'
            
            Else, read from tower file path provided.
            
    Output:
        
        Numpy array of tower coordinates (long, lat).
        
    """
    
    if not filename:
        df = pd.read_csv('https://msu-cmse-courses.github.io/cmse495-SS22/Files/TowersRVN479554GVQ144174.csv')
    else:
        df = pd.read_csv(filename)
        
    return df[['longitude','latitude']].values